(function($){
    var typed = new Typed('span.txt-rotate',{
        strings: ["I'm a Web Developer", "I'm a Worldpress Developer", "I'm a Graphic Designer"],
        typeSpeed: 100,
        backSpeed: 100,
        fadeOut: false,
        smartBackspace: true,
        loop: true
    });
})(jQuery);