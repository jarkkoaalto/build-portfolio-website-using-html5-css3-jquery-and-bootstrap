# Build Portfolio Website Using HTML5, CSS3, jQuery and Bootstrap #

## Help references ##
- Bootstrap: https://getbootstrap.com/docs/4.4/getting-started/download/
- jQuery: https://code.jquery.com/jquery-3.4.1.min.js
- normalize.css: http://necolas.github.io/normalize.css/8.0.1/normalize.css
- material design Icons: https://materialdesignicons.com/
- Typed-js: https://mattboldt.com/demos/typed-js/

![Screenshot](example.png)